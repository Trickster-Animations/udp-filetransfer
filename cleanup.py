"""
Cleanup the repo according the the .gitignore file
"""

import os
from shutil import rmtree
import fnmatch

with open(".gitignore") as ignorefile:
    lines = ignorefile.readlines()
    cleanup_exp = []
    try:
        batch = []
        while True:
            line = lines.pop().strip()
            if line != "":
                batch.append(line)
            else:
                cleanup_exp.extend(batch)
                batch = []
    except IndexError:
        pass
print(cleanup_exp)

for root, dirs, files in os.walk("."):
    for f in files:
        for exp in cleanup_exp:
            if fnmatch.fnmatch(os.path.join(root, f), exp):
                os.remove(os.path.join(root, f))
    for d in dirs:
        for exp in cleanup_exp:
            if fnmatch.fnmatch(os.path.join(root, d), exp):
                rmtree(os.path.join(root, d))