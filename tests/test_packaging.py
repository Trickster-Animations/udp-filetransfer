from udp_filetransfer import __version__
from toml import load
from os import getenv
import semver
import pytest

def test_version_tag():
    tag = getenv("CI_COMMIT_TAG")
    if tag is None:
        pytest.skip()
    assert semver.compare(tag.strip("v"), __version__) == 0


def test_version_poetry():
    ver = load("pyproject.toml")["tool"]["poetry"]["version"]
    assert __version__ == ver