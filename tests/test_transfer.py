from threading import Thread
from udp_filetransfer import send, receive
from hashlib import md5
from tempfile import NamedTemporaryFile
from string import printable
from random import randrange
from os import remove
from pytest import raises
from time import sleep

result = ""

def receiver_wrapper():
    global result
    result = receive()


def test_transfer():
    global result
    with NamedTemporaryFile("w+", delete=False) as testfile:
        for _ in range(1024*1024*5):
            testfile.write(printable[randrange(0, len(printable))])
    receiver_thread = Thread(target=receiver_wrapper)
    sender_thread = Thread(target=send, args=(testfile.name,))
    receiver_thread.start()
    sleep(10)
    sender_thread.start()
    sender_thread.join()
    receiver_thread.join(10)
    assert not receiver_thread.is_alive()
    src_hash = md5()
    copy_hash = md5()
    with open(testfile.name, "rb") as src:
        data = src.read(1024)
        while data:
            src_hash.update(data)
            data = src.read(1024)
    with open(result, "rb") as copy:
        data = copy.read(1024)
        while data:
            copy_hash.update(data)
            data = copy.read(1024)
    assert src_hash.digest() == copy_hash.digest()
    remove(testfile.name)
    remove(result)


def test_noreceiver():
    with NamedTemporaryFile("w+", delete=False) as testfile:
        for _ in range(1024*1024*5):
            testfile.write(printable[randrange(0, len(printable))])
    with raises(TimeoutError):
        send(testfile.name)
    remove(testfile.name)